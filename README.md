<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="generator"  content="lshw-unknown" />
<style type="text/css">
  .first {font-weight: bold; margin-left: none; padding-right: 1em;vertical-align: top; }
  .second {padding-left: 1em; width: 100%; vertical-align: center; }
  .id {font-family: monospace;}
  .indented {margin-left: 2em; border-left: dotted thin #dde; padding-bottom: 1em; }
  .node {border: solid thin #ffcc66; padding: 1em; background: #ffffcc; }
  .node-unclaimed {border: dotted thin #c3c3c3; padding: 1em; background: #fafafa; color: red; }
  .node-disabled {border: solid thin #f55; padding: 1em; background: #fee; color: gray; }
</style>
<title>djole-ms-7721</title>
</head>
<body>
<div class="indented">
<table width="100%" class="node" summary="attributes of djole-ms-7721">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">djole-ms-7721</div></td></tr></thead>
 <tbody>
    <tr><td class="first">description: </td><td class="second">Desktop Computer</td></tr>
    <tr><td class="first">product: </td><td class="second">MS-7721 (To be filled by O.E.M.)</td></tr>
    <tr><td class="first">vendor: </td><td class="second">MSI</td></tr>
    <tr><td class="first">version: </td><td class="second">2.0</td></tr>
    <tr><td class="first">serial: </td><td class="second">To be filled by O.E.M.</td></tr>
    <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
    <tr><td class="first">capabilities: </td><td class="second"><dfn title="SMBIOS version 2.7">smbios-2.7</dfn> <dfn title="DMI version 2.7">dmi-2.7</dfn> <dfn title="Symmetric Multi-Processing">smp</dfn> <dfn title="32-bit processes">vsyscall32</dfn> </td></tr>
    <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of djole-ms-7721"><tr><td class="sub-first"> boot</td><td>=</td><td>normal</td></tr><tr><td class="sub-first"> chassis</td><td>=</td><td>desktop</td></tr><tr><td class="sub-first"> family</td><td>=</td><td>To be filled by O.E.M.</td></tr><tr><td class="sub-first"> sku</td><td>=</td><td>To be filled by O.E.M.</td></tr><tr><td class="sub-first"> uuid</td><td>=</td><td>00000000-0000-0000-0000-448A5B25B965</td></tr></table></td></tr>
 </tbody></table></div>
<div class="indented">
        <div class="indented">
    <table width="100%" class="node" summary="attributes of core">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">core</div></td></tr></thead>
 <tbody>
       <tr><td class="first">description: </td><td class="second">Motherboard</td></tr>
       <tr><td class="first">product: </td><td class="second">FM2-A55M-E33 (MS-7721)</td></tr>
       <tr><td class="first">vendor: </td><td class="second">MSI</td></tr>
       <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
       <tr><td class="first">version: </td><td class="second">2.0</td></tr>
       <tr><td class="first">serial: </td><td class="second">To be filled by O.E.M.</td></tr>
       <tr><td class="first">slot: </td><td class="second">To be filled by O.E.M.</td></tr>
 </tbody>    </table></div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of firmware">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">firmware</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">BIOS</td></tr>
          <tr><td class="first">vendor: </td><td class="second">American Megatrends Inc.</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
          <tr><td class="first">version: </td><td class="second">V11.4</td></tr>
          <tr><td class="first">date: </td><td class="second">10/12/2013</td></tr>
          <tr><td class="first">size: </td><td class="second">64KiB</td></tr>
          <tr><td class="first">capacity: </td><td class="second">8128KiB</td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="PCI bus">pci</dfn> <dfn title="BIOS EEPROM can be upgraded">upgrade</dfn> <dfn title="BIOS shadowing">shadowing</dfn> <dfn title="Booting from CD-ROM/DVD">cdboot</dfn> <dfn title="Selectable boot path">bootselect</dfn> <dfn title="BIOS ROM is socketed">socketedrom</dfn> <dfn title="Enhanced Disk Drive extensions">edd</dfn> <dfn title="5.25&quot; 1.2MB floppy">int13floppy1200</dfn> <dfn title="3.5&quot; 720KB floppy">int13floppy720</dfn> <dfn title="3.5&quot; 2.88MB floppy">int13floppy2880</dfn> <dfn title="Print Screen key">int5printscreen</dfn> <dfn title="i8042 keyboard controller">int9keyboard</dfn> <dfn title="INT14 serial line control">int14serial</dfn> <dfn title="INT17 printer control">int17printer</dfn> <dfn title="ACPI">acpi</dfn> <dfn title="USB legacy emulation">usb</dfn> <dfn title="BIOS boot specification">biosbootspecification</dfn> <dfn title="UEFI specification is supported">uefi</dfn> </td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of cache:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">cache:0</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">L1 cache</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">23</div></td></tr>
          <tr><td class="first">slot: </td><td class="second">L1 CACHE</td></tr>
          <tr><td class="first">size: </td><td class="second">192KiB</td></tr>
          <tr><td class="first">capacity: </td><td class="second">192KiB</td></tr>
          <tr><td class="first">clock: </td><td class="second">1GHz (1.0ns)</td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="Pipeline burst">pipeline-burst</dfn> <dfn title="Internal">internal</dfn> <dfn title="Write-back">write-back</dfn> <dfn title="Unified cache">unified</dfn> </td></tr>
          <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of cache:0"><tr><td class="sub-first"> level</td><td>=</td><td>1</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of cache:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">cache:1</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">L2 cache</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">24</div></td></tr>
          <tr><td class="first">slot: </td><td class="second">L2 CACHE</td></tr>
          <tr><td class="first">size: </td><td class="second">4MiB</td></tr>
          <tr><td class="first">capacity: </td><td class="second">4MiB</td></tr>
          <tr><td class="first">clock: </td><td class="second">1GHz (1.0ns)</td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="Pipeline burst">pipeline-burst</dfn> <dfn title="Internal">internal</dfn> <dfn title="Write-back">write-back</dfn> <dfn title="Unified cache">unified</dfn> </td></tr>
          <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of cache:1"><tr><td class="sub-first"> level</td><td>=</td><td>2</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of memory">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">memory</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">System Memory</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">32</div></td></tr>
          <tr><td class="first">slot: </td><td class="second">System board or motherboard</td></tr>
          <tr><td class="first">size: </td><td class="second">16GiB</td></tr>
 </tbody>       </table></div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of bank:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">bank:0</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">DIMM DDR3 Synchronous Unbuffered (Unregistered) 1600 MHz (0,6 ns)</td></tr>
             <tr><td class="first">product: </td><td class="second">KHX1866C9D3/8GX</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Kingston</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
             <tr><td class="first">serial: </td><td class="second">1126BE9A</td></tr>
             <tr><td class="first">slot: </td><td class="second">DIMM 1</td></tr>
             <tr><td class="first">size: </td><td class="second">8GiB</td></tr>
             <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">1600MHz (0.6ns)</td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of bank:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">bank:1</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">DIMM DDR3 Synchronous Unbuffered (Unregistered) 1866 MHz (0,5 ns)</td></tr>
             <tr><td class="first">product: </td><td class="second">KHX1866C10D3/8G</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Kingston</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
             <tr><td class="first">serial: </td><td class="second">4D2CC90B</td></tr>
             <tr><td class="first">slot: </td><td class="second">DIMM 1</td></tr>
             <tr><td class="first">size: </td><td class="second">8GiB</td></tr>
             <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">1866MHz (0.5ns)</td></tr>
 </tbody>          </table></div>
          </div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of cpu">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">cpu</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">CPU</td></tr>
          <tr><td class="first">product: </td><td class="second">AMD A8-6600K APU with Radeon(tm) HD Graphics</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">3a</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">cpu@0</div></td></tr>
          <tr><td class="first">version: </td><td class="second">AMD A8-6600K APU with Radeon(tm) HD Graphics</td></tr>
          <tr><td class="first">slot: </td><td class="second">P0</td></tr>
          <tr><td class="first">size: </td><td class="second">1990MHz</td></tr>
          <tr><td class="first">capacity: </td><td class="second">3900MHz</td></tr>
          <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">100MHz</td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="64bits extensions (x86-64)">x86-64</dfn> <dfn title="mathematical co-processor">fpu</dfn> <dfn title="FPU exceptions reporting">fpu_exception</dfn> <dfn title="">wp</dfn> <dfn title="virtual mode extensions">vme</dfn> <dfn title="debugging extensions">de</dfn> <dfn title="page size extensions">pse</dfn> <dfn title="time stamp counter">tsc</dfn> <dfn title="model-specific registers">msr</dfn> <dfn title="4GB+ memory addressing (Physical Address Extension)">pae</dfn> <dfn title="machine check exceptions">mce</dfn> <dfn title="compare and exchange 8-byte">cx8</dfn> <dfn title="on-chip advanced programmable interrupt controller (APIC)">apic</dfn> <dfn title="fast system calls">sep</dfn> <dfn title="memory type range registers">mtrr</dfn> <dfn title="page global enable">pge</dfn> <dfn title="machine check architecture">mca</dfn> <dfn title="conditional move instruction">cmov</dfn> <dfn title="page attribute table">pat</dfn> <dfn title="36-bit page size extensions">pse36</dfn> <dfn title="">clflush</dfn> <dfn title="multimedia extensions (MMX)">mmx</dfn> <dfn title="fast floating point save/restore">fxsr</dfn> <dfn title="streaming SIMD extensions (SSE)">sse</dfn> <dfn title="streaming SIMD extensions (SSE2)">sse2</dfn> <dfn title="HyperThreading">ht</dfn> <dfn title="fast system calls">syscall</dfn> <dfn title="no-execute bit (NX)">nx</dfn> <dfn title="multimedia extensions (MMXExt)">mmxext</dfn> <dfn title="">fxsr_opt</dfn> <dfn title="">pdpe1gb</dfn> <dfn title="">rdtscp</dfn> <dfn title="">constant_tsc</dfn> <dfn title="">rep_good</dfn> <dfn title="">nopl</dfn> <dfn title="">nonstop_tsc</dfn> <dfn title="">cpuid</dfn> <dfn title="">extd_apicid</dfn> <dfn title="">aperfmperf</dfn> <dfn title="">pni</dfn> <dfn title="">pclmulqdq</dfn> <dfn title="">monitor</dfn> <dfn title="">ssse3</dfn> <dfn title="">fma</dfn> <dfn title="">cx16</dfn> <dfn title="">sse4_1</dfn> <dfn title="">sse4_2</dfn> <dfn title="">popcnt</dfn> <dfn title="">aes</dfn> <dfn title="">xsave</dfn> <dfn title="">avx</dfn> <dfn title="">f16c</dfn> <dfn title="">lahf_lm</dfn> <dfn title="">cmp_legacy</dfn> <dfn title="">svm</dfn> <dfn title="">extapic</dfn> <dfn title="">cr8_legacy</dfn> <dfn title="">abm</dfn> <dfn title="">sse4a</dfn> <dfn title="">misalignsse</dfn> <dfn title="">3dnowprefetch</dfn> <dfn title="">osvw</dfn> <dfn title="">ibs</dfn> <dfn title="">xop</dfn> <dfn title="">skinit</dfn> <dfn title="">wdt</dfn> <dfn title="">lwp</dfn> <dfn title="">fma4</dfn> <dfn title="">tce</dfn> <dfn title="">nodeid_msr</dfn> <dfn title="">tbm</dfn> <dfn title="">topoext</dfn> <dfn title="">perfctr_core</dfn> <dfn title="">perfctr_nb</dfn> <dfn title="">cpb</dfn> <dfn title="">hw_pstate</dfn> <dfn title="">vmmcall</dfn> <dfn title="">bmi1</dfn> <dfn title="">arat</dfn> <dfn title="">npt</dfn> <dfn title="">lbrv</dfn> <dfn title="">svm_lock</dfn> <dfn title="">nrip_save</dfn> <dfn title="">tsc_scale</dfn> <dfn title="">vmcb_clean</dfn> <dfn title="">flushbyasid</dfn> <dfn title="">decodeassists</dfn> <dfn title="">pausefilter</dfn> <dfn title="">pfthreshold</dfn> <dfn title="CPU Frequency scaling">cpufreq</dfn> </td></tr>
          <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of cpu"><tr><td class="sub-first"> cores</td><td>=</td><td>4</td></tr><tr><td class="sub-first"> enabledcores</td><td>=</td><td>4</td></tr><tr><td class="sub-first"> threads</td><td>=</td><td>4</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:0</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Family 15h (Models 10h-1fh) Processor Root Complex</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">100</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:00.0</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">66MHz</td></tr>
 </tbody>       </table></div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node-unclaimed" summary="attributes of generic">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">generic</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">IOMMU</td></tr>
             <tr><td class="first">product: </td><td class="second">Family 15h (Models 10h-1fh) I/O Memory Management Unit</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">0.2</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:00.2</div></td></tr>
             <tr><td class="first">version: </td><td class="second">00</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="HyperTransport">ht</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of generic"><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of pci:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:0</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">PCI bridge</td></tr>
             <tr><td class="first">product: </td><td class="second">Family 15h (Models 10h-1fh) Processor Root Port</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">2</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:02.0</div></td></tr>
             <tr><td class="first">version: </td><td class="second">00</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pci</dfn> <dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="HyperTransport">ht</dfn> <dfn title="">normal_decode</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pci:0"><tr><td class="sub-first"> driver</td><td>=</td><td>pcieport</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of pci:0"><tr><td class="sub-first"> irq</td><td>:</td><td>18</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>e000(size=4096)</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fea00000-feafffff</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>c0000000(size=268435456)</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of display">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">display</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">VGA compatible controller</td></tr>
                <tr><td class="first">product: </td><td class="second">Caicos PRO [Radeon HD 7450]</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD/ATI]</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:01:00.0</div></td></tr>
                <tr><td class="first">version: </td><td class="second">00</td></tr>
                <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
                <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="">vga_controller</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> <dfn title="extension ROM">rom</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of display"><tr><td class="sub-first"> driver</td><td>=</td><td>radeon</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
                <tr><td class="first">resources:</td><td class="second"><table summary="resources of display"><tr><td class="sub-first"> irq</td><td>:</td><td>34</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>c0000000-cfffffff</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fea20000-fea3ffff</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>e000(size=256)</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>c0000-dffff</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of multimedia">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">multimedia</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">Audio device</td></tr>
                <tr><td class="first">product: </td><td class="second">Caicos HDMI Audio [Radeon HD 6450 / 7450/8450/8490 OEM / R5 230/235/235X OEM]</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD/ATI]</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">0.1</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:01:00.1</div></td></tr>
                <tr><td class="first">version: </td><td class="second">00</td></tr>
                <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
                <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of multimedia"><tr><td class="sub-first"> driver</td><td>=</td><td>snd_hda_intel</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
                <tr><td class="first">resources:</td><td class="second"><table summary="resources of multimedia"><tr><td class="sub-first"> irq</td><td>:</td><td>35</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fea40000-fea43fff</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of pci:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:1</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">PCI bridge</td></tr>
             <tr><td class="first">product: </td><td class="second">Family 15h (Models 10h-1fh) Processor Root Port</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">4</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:04.0</div></td></tr>
             <tr><td class="first">version: </td><td class="second">00</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pci</dfn> <dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="HyperTransport">ht</dfn> <dfn title="">normal_decode</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pci:1"><tr><td class="sub-first"> driver</td><td>=</td><td>pcieport</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of pci:1"><tr><td class="sub-first"> irq</td><td>:</td><td>16</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>d000(size=4096)</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>d0000000(size=1048576)</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of network">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">network</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">Ethernet interface</td></tr>
                <tr><td class="first">product: </td><td class="second">RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Realtek Semiconductor Co., Ltd.</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:02:00.0</div></td></tr>
                <tr><td class="first">logical name: </td><td class="second"><div class="id">enp2s0</div></td></tr>
                <tr><td class="first">version: </td><td class="second">06</td></tr>
                <tr><td class="first">serial: </td><td class="second">44:8a:5b:25:b9:65</td></tr>
                <tr><td class="first">size: </td><td class="second">1Gbit/s</td></tr>
                <tr><td class="first">capacity: </td><td class="second">1Gbit/s</td></tr>
                <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
                <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="MSI-X">msix</dfn> <dfn title="Vital Product Data">vpd</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> <dfn title="">ethernet</dfn> <dfn title="Physical interface">physical</dfn> <dfn title="twisted pair">tp</dfn> <dfn title="Media Independent Interface">mii</dfn> <dfn title="10Mbit/s">10bt</dfn> <dfn title="10Mbit/s (full duplex)">10bt-fd</dfn> <dfn title="100Mbit/s">100bt</dfn> <dfn title="100Mbit/s (full duplex)">100bt-fd</dfn> <dfn title="1Gbit/s">1000bt</dfn> <dfn title="1Gbit/s (full duplex)">1000bt-fd</dfn> <dfn title="Auto-negotiation">autonegotiation</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of network"><tr><td class="sub-first"> autonegotiation</td><td>=</td><td>on</td></tr><tr><td class="sub-first"> broadcast</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> driver</td><td>=</td><td>r8169</td></tr><tr><td class="sub-first"> driverversion</td><td>=</td><td>2.3LK-NAPI</td></tr><tr><td class="sub-first"> duplex</td><td>=</td><td>full</td></tr><tr><td class="sub-first"> firmware</td><td>=</td><td>rtl8168e-3_0.0.4 03/27/12</td></tr><tr><td class="sub-first"> ip</td><td>=</td><td>192.168.1.154</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr><tr><td class="sub-first"> link</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> multicast</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> port</td><td>=</td><td>MII</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>1Gbit/s</td></tr></table></td></tr>
                <tr><td class="first">resources:</td><td class="second"><table summary="resources of network"><tr><td class="sub-first"> irq</td><td>:</td><td>25</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>d000(size=256)</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>d0004000-d0004fff</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>d0000000-d0003fff</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of storage">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">storage</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">SATA controller</td></tr>
             <tr><td class="first">product: </td><td class="second">FCH SATA Controller [AHCI mode]</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">11</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:11.0</div></td></tr>
             <tr><td class="first">version: </td><td class="second">40</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="">storage</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="">ahci_1.0</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of storage"><tr><td class="sub-first"> driver</td><td>=</td><td>ahci</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>32</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of storage"><tr><td class="sub-first"> irq</td><td>:</td><td>26</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>f040(size=8)</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>f030(size=4)</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>f020(size=8)</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>f010(size=4)</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>f000(size=16)</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>feb0b000-feb0b7ff</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of usb:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:0</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">USB controller</td></tr>
             <tr><td class="first">product: </td><td class="second">FCH USB OHCI Controller</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">12</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:12.0</div></td></tr>
             <tr><td class="first">version: </td><td class="second">11</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="Open Host Controller Interface">ohci</dfn> <dfn title="bus mastering">bus_master</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:0"><tr><td class="sub-first"> driver</td><td>=</td><td>ohci-pci</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>32</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of usb:0"><tr><td class="sub-first"> irq</td><td>:</td><td>18</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>feb0a000-feb0afff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of usbhost">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usbhost</div></td></tr></thead>
 <tbody>
                <tr><td class="first">product: </td><td class="second">OHCI PCI host controller</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Linux 4.15.0-20-generic ohci_hcd</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@4</div></td></tr>
                <tr><td class="first">logical name: </td><td class="second"><div class="id">usb4</div></td></tr>
                <tr><td class="first">version: </td><td class="second">4.15</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 1.1">usb-1.10</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usbhost"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>5</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>12Mbit/s</td></tr></table></td></tr>
 </tbody>             </table></div>
<div class="indented">
                                <div class="indented">
                <table width="100%" class="node" summary="attributes of usb">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb</div></td></tr></thead>
 <tbody>
                   <tr><td class="first">description: </td><td class="second">Mouse</td></tr>
                   <tr><td class="first">product: </td><td class="second">USB Optical Mouse</td></tr>
                   <tr><td class="first">vendor: </td><td class="second">Logitech</td></tr>
                   <tr><td class="first">physical id: </td><td class="second"><div class="id">5</div></td></tr>
                   <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@4:5</div></td></tr>
                   <tr><td class="first">version: </td><td class="second">72.00</td></tr>
                   <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                   <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb"><tr><td class="sub-first"> driver</td><td>=</td><td>usbhid</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>100mA</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>1Mbit/s</td></tr></table></td></tr>
 </tbody>                </table></div>
                </div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of usb:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:1</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">USB controller</td></tr>
             <tr><td class="first">product: </td><td class="second">FCH USB EHCI Controller</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">12.2</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:12.2</div></td></tr>
             <tr><td class="first">version: </td><td class="second">11</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="Debug port">debug</dfn> <dfn title="Enhanced Host Controller Interface (USB2)">ehci</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:1"><tr><td class="sub-first"> driver</td><td>=</td><td>ehci-pci</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>32</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of usb:1"><tr><td class="sub-first"> irq</td><td>:</td><td>17</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>feb09000-feb090ff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of usbhost">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usbhost</div></td></tr></thead>
 <tbody>
                <tr><td class="first">product: </td><td class="second">EHCI Host Controller</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Linux 4.15.0-20-generic ehci_hcd</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@1</div></td></tr>
                <tr><td class="first">logical name: </td><td class="second"><div class="id">usb1</div></td></tr>
                <tr><td class="first">version: </td><td class="second">4.15</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usbhost"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>5</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>480Mbit/s</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of usb:2">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:2</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">USB controller</td></tr>
             <tr><td class="first">product: </td><td class="second">FCH USB OHCI Controller</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">13</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:13.0</div></td></tr>
             <tr><td class="first">version: </td><td class="second">11</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="Open Host Controller Interface">ohci</dfn> <dfn title="bus mastering">bus_master</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:2"><tr><td class="sub-first"> driver</td><td>=</td><td>ohci-pci</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>32</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of usb:2"><tr><td class="sub-first"> irq</td><td>:</td><td>18</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>feb08000-feb08fff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of usbhost">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usbhost</div></td></tr></thead>
 <tbody>
                <tr><td class="first">product: </td><td class="second">OHCI PCI host controller</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Linux 4.15.0-20-generic ohci_hcd</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@5</div></td></tr>
                <tr><td class="first">logical name: </td><td class="second"><div class="id">usb5</div></td></tr>
                <tr><td class="first">version: </td><td class="second">4.15</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 1.1">usb-1.10</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usbhost"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>5</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>12Mbit/s</td></tr></table></td></tr>
 </tbody>             </table></div>
<div class="indented">
                                <div class="indented">
                <table width="100%" class="node" summary="attributes of usb">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb</div></td></tr></thead>
 <tbody>
                   <tr><td class="first">description: </td><td class="second">Keyboard</td></tr>
                   <tr><td class="first">product: </td><td class="second">USB Keyboard</td></tr>
                   <tr><td class="first">vendor: </td><td class="second">Logitech</td></tr>
                   <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                   <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@5:1</div></td></tr>
                   <tr><td class="first">version: </td><td class="second">64.00</td></tr>
                   <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 1.1">usb-1.10</dfn> </td></tr>
                   <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb"><tr><td class="sub-first"> driver</td><td>=</td><td>usbhid</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>90mA</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>1Mbit/s</td></tr></table></td></tr>
 </tbody>                </table></div>
                </div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of usb:3">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:3</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">USB controller</td></tr>
             <tr><td class="first">product: </td><td class="second">FCH USB EHCI Controller</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">13.2</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:13.2</div></td></tr>
             <tr><td class="first">version: </td><td class="second">11</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="Debug port">debug</dfn> <dfn title="Enhanced Host Controller Interface (USB2)">ehci</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:3"><tr><td class="sub-first"> driver</td><td>=</td><td>ehci-pci</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>32</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of usb:3"><tr><td class="sub-first"> irq</td><td>:</td><td>17</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>feb07000-feb070ff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of usbhost">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usbhost</div></td></tr></thead>
 <tbody>
                <tr><td class="first">product: </td><td class="second">EHCI Host Controller</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Linux 4.15.0-20-generic ehci_hcd</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@2</div></td></tr>
                <tr><td class="first">logical name: </td><td class="second"><div class="id">usb2</div></td></tr>
                <tr><td class="first">version: </td><td class="second">4.15</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usbhost"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>5</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>480Mbit/s</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of serial">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">serial</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">SMBus</td></tr>
             <tr><td class="first">product: </td><td class="second">FCH SMBus Controller</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">14</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:14.0</div></td></tr>
             <tr><td class="first">version: </td><td class="second">14</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of serial"><tr><td class="sub-first"> driver</td><td>=</td><td>piix4_smbus</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of serial"><tr><td class="sub-first"> irq</td><td>:</td><td>0</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of multimedia">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">multimedia</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">Audio device</td></tr>
             <tr><td class="first">product: </td><td class="second">FCH Azalia Controller</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">14.2</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:14.2</div></td></tr>
             <tr><td class="first">version: </td><td class="second">01</td></tr>
             <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of multimedia"><tr><td class="sub-first"> driver</td><td>=</td><td>snd_hda_intel</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>32</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of multimedia"><tr><td class="sub-first"> irq</td><td>:</td><td>16</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>feb00000-feb03fff</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of isa">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">isa</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">ISA bridge</td></tr>
             <tr><td class="first">product: </td><td class="second">FCH LPC Bridge</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">14.3</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:14.3</div></td></tr>
             <tr><td class="first">version: </td><td class="second">11</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="">isa</dfn> <dfn title="bus mastering">bus_master</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of isa"><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of pci:2">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:2</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">PCI bridge</td></tr>
             <tr><td class="first">product: </td><td class="second">FCH PCI Bridge</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">14.4</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:14.4</div></td></tr>
             <tr><td class="first">version: </td><td class="second">40</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pci</dfn> <dfn title="">subtractive_decode</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="VGA palette">vga_palette</dfn> </td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of usb:4">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:4</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">USB controller</td></tr>
             <tr><td class="first">product: </td><td class="second">FCH USB OHCI Controller</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">14.5</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:14.5</div></td></tr>
             <tr><td class="first">version: </td><td class="second">11</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="Open Host Controller Interface">ohci</dfn> <dfn title="bus mastering">bus_master</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:4"><tr><td class="sub-first"> driver</td><td>=</td><td>ohci-pci</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>32</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of usb:4"><tr><td class="sub-first"> irq</td><td>:</td><td>18</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>feb06000-feb06fff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of usbhost">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usbhost</div></td></tr></thead>
 <tbody>
                <tr><td class="first">product: </td><td class="second">OHCI PCI host controller</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Linux 4.15.0-20-generic ohci_hcd</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@6</div></td></tr>
                <tr><td class="first">logical name: </td><td class="second"><div class="id">usb6</div></td></tr>
                <tr><td class="first">version: </td><td class="second">4.15</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 1.1">usb-1.10</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usbhost"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>2</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>12Mbit/s</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of usb:5">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:5</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">USB controller</td></tr>
             <tr><td class="first">product: </td><td class="second">FCH USB OHCI Controller</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">16</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:16.0</div></td></tr>
             <tr><td class="first">version: </td><td class="second">11</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="Open Host Controller Interface">ohci</dfn> <dfn title="bus mastering">bus_master</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:5"><tr><td class="sub-first"> driver</td><td>=</td><td>ohci-pci</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>32</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of usb:5"><tr><td class="sub-first"> irq</td><td>:</td><td>18</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>feb05000-feb05fff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of usbhost">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usbhost</div></td></tr></thead>
 <tbody>
                <tr><td class="first">product: </td><td class="second">OHCI PCI host controller</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Linux 4.15.0-20-generic ohci_hcd</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@7</div></td></tr>
                <tr><td class="first">logical name: </td><td class="second"><div class="id">usb7</div></td></tr>
                <tr><td class="first">version: </td><td class="second">4.15</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 1.1">usb-1.10</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usbhost"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>4</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>12Mbit/s</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of usb:6">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:6</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">USB controller</td></tr>
             <tr><td class="first">product: </td><td class="second">FCH USB EHCI Controller</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">16.2</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:16.2</div></td></tr>
             <tr><td class="first">version: </td><td class="second">11</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="Debug port">debug</dfn> <dfn title="Enhanced Host Controller Interface (USB2)">ehci</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:6"><tr><td class="sub-first"> driver</td><td>=</td><td>ehci-pci</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>32</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of usb:6"><tr><td class="sub-first"> irq</td><td>:</td><td>17</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>feb04000-feb040ff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of usbhost">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usbhost</div></td></tr></thead>
 <tbody>
                <tr><td class="first">product: </td><td class="second">EHCI Host Controller</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Linux 4.15.0-20-generic ehci_hcd</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@3</div></td></tr>
                <tr><td class="first">logical name: </td><td class="second"><div class="id">usb3</div></td></tr>
                <tr><td class="first">version: </td><td class="second">4.15</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usbhost"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>4</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>480Mbit/s</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:1</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Family 15h (Models 10h-1fh) Processor Function 0</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">101</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:18.0</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:2">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:2</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Family 15h (Models 10h-1fh) Processor Function 1</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">102</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:18.1</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:3">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:3</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Family 15h (Models 10h-1fh) Processor Function 2</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">103</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:18.2</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:4">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:4</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Family 15h (Models 10h-1fh) Processor Function 3</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">104</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:18.3</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
          <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pci:4"><tr><td class="sub-first"> driver</td><td>=</td><td>k10temp</td></tr></table></td></tr>
          <tr><td class="first">resources:</td><td class="second"><table summary="resources of pci:4"><tr><td class="sub-first"> irq</td><td>:</td><td>0</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:5">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:5</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Family 15h (Models 10h-1fh) Processor Function 4</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">105</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:18.4</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:6">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:6</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Family 15h (Models 10h-1fh) Processor Function 5</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">106</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:18.5</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of scsi">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">scsi</div></td></tr></thead>
 <tbody>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
          <tr><td class="first">logical name: </td><td class="second"><div class="id">scsi1</div></td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="Emulated device">emulated</dfn> </td></tr>
 </tbody>       </table></div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of disk">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">disk</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">ATA Disk</td></tr>
             <tr><td class="first">product: </td><td class="second">SPCC Solid State</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">0.0.0</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">scsi@1:0.0.0</div></td></tr>
             <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/sda</div></td></tr>
             <tr><td class="first">version: </td><td class="second">V3.3</td></tr>
             <tr><td class="first">serial: </td><td class="second">P1700076000000000844</td></tr>
             <tr><td class="first">size: </td><td class="second">111GiB (120GB)</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="Partitioned disk">partitioned</dfn> <dfn title="MS-DOS partition table">partitioned:dos</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of disk"><tr><td class="sub-first"> ansiversion</td><td>=</td><td>5</td></tr><tr><td class="sub-first"> logicalsectorsize</td><td>=</td><td>512</td></tr><tr><td class="sub-first"> sectorsize</td><td>=</td><td>512</td></tr><tr><td class="sub-first"> signature</td><td>=</td><td>434972c3</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of volume">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">volume</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">EXT4 volume</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Linux</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">scsi@1:0.0.0,1</div></td></tr>
                <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/sda1</div></td></tr>
                <tr><td class="first">logical name: </td><td class="second"><div class="id">/</div></td></tr>
                <tr><td class="first">version: </td><td class="second">1.0</td></tr>
                <tr><td class="first">serial: </td><td class="second">2c156f52-a835-4829-8e4d-1efa6cbcbdd4</td></tr>
                <tr><td class="first">size: </td><td class="second">111GiB</td></tr>
                <tr><td class="first">capacity: </td><td class="second">111GiB</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="Primary partition">primary</dfn> <dfn title="Bootable partition (active)">bootable</dfn> <dfn title="">journaled</dfn> <dfn title="Extended Attributes">extended_attributes</dfn> <dfn title="4GB+ files">large_files</dfn> <dfn title="16TB+ files">huge_files</dfn> <dfn title="directories with 65000+ subdirs">dir_nlink</dfn> <dfn title="needs recovery">recover</dfn> <dfn title="64bit filesystem">64bit</dfn> <dfn title="extent-based allocation">extents</dfn> <dfn title="">ext4</dfn> <dfn title="EXT2/EXT3">ext2</dfn> <dfn title="initialized volume">initialized</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of volume"><tr><td class="sub-first"> created</td><td>=</td><td>2018-07-25 16:57:52</td></tr><tr><td class="sub-first"> filesystem</td><td>=</td><td>ext4</td></tr><tr><td class="sub-first"> lastmountpoint</td><td>=</td><td>/</td></tr><tr><td class="sub-first"> modified</td><td>=</td><td>2018-07-25 17:39:51</td></tr><tr><td class="sub-first"> mount.fstype</td><td>=</td><td>ext4</td></tr><tr><td class="sub-first"> mount.options</td><td>=</td><td>rw,relatime,errors=remount-ro,data=ordered</td></tr><tr><td class="sub-first"> mounted</td><td>=</td><td>2018-07-25 17:39:52</td></tr><tr><td class="sub-first"> state</td><td>=</td><td>mounted</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
       </div>
    </div>
</body>
</html>
